//Import the contents of th Express package to use for our application

const express = require('express');

//Mongoose is an ODM (Object Document Mapper) library for MongoDB and Node.js that manages models/schemas and allows a quick and easy way to connect to a MongoDB database.

const mongoose = require('mongoose');

//Give the express() function from the Express package a variable "app" so that it can be called more easily

const app = express();

//Mongoose's connect method takes our MongoDB Atlas connection string and uses it to connect to Atlas and authenticate our credentials, as well as specifies the database that our app needs to use

//useNewUrlParser and useUnifiedTopology are both set to true as part of a newer Mongoose update that allows a more efficient way to connect to Atlas, since the older way is about to be deprecated (become obsolete)

mongoose.connect("mongodb+srv://admin:admin@testdatabase1.zq0wb.mongodb.net/b153_tasks?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

//confirm that Atlas connection is successful

/*
mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas."))
*/

//mongoose.connection is used to handle error or successful connection to MongoDB

let db = mongoose.connection;

//check mongoose.connection on error

db.on("error", console.error.bind(console, "connection error"));

//confirmation that connection to MongoDB is successful.

db.once("open",()=>console.log("We are now connected to MongoDB"));

//middleware - are functions/ methods we use in our server/api
//This middlewarefunctions serve as gates/it allows us to other taks:
//This will allow us to handle the request body json

app.use(express.json());


//Declare a port number variable

const port = 4000;

//Create a GET route to check if Express is working: '/' => depicts an endpoint

app.get('/', (req, res) => {
	res.send("Hello from Express!"); // res.send() - sends the response to the browser
})

/*
	Mongoose Schema:

	Before we can create documents in our database, we first have to declare a "blueprint" or "template" of our documents. This is to ensure that the content/ fields of documents are uniform. 

	Gone are the days when we have to worry if we correctly spelled the fields in our documents.

	Schema acts as a blueprint of our data/document.

	It is a representation of how our documents are structured. It also determmines the types of data and the expected fields/ properties per document. 
*/

//Schema() is a constructor from mongoose that will allow us to create a new schema

const taskSchema = new mongoose.Schema({
	/*
		Define the fields for the task document.

		The task document should have a name and a status field.

		Both fields MUST be strings.
	*/

	name: String,
	status: String 
})

/*
	Sample task document based on schema

	{
		name: "Sample Task",
		status: "Complete"
	}
*/


/*
	Mongoose Model

	Models are used to connect yuor API to the corresponding collection in your database. it is a representation of your documents.

	Models uses schemas to create objects/ documents that corresponds to the schema. By default, when creating the collection from your model, the collection name is pluralized in MongoDB.

	mongoose.model(<nameOfCollection>,<schemaToFollow>)
*/

let Task = mongoose.model("Task",taskSchema);

//mongodb equivalent of Task = db.tasks

//POST route - add task documents:

app.post('/',(req,res)=>{

	//check the incoming request body

	//console.log(req.body)

	//req.body in terminal
	//{ name: 'Learn NodeJS', status: 'pending' }


	//create a new task document from our model
	// The values for the documents we are going to create will come from req.body
	//req.body is an object

	let newTask = new Task({
		name: req.body.name,
		status: req.body.status
	})

	//newTask is a mongoose document/object
	//console.log(newTask);

	//save() is a ,ethod from an object/document created by a model
	//This method will allow us to sabe our document in our tasks collection
	//.then() and .catch() chain
	/*
		.then() is used to handle the result/return of a function. If the method/ function properly returns a value, we can run a separate function to handle it.

		.catch() is used to catch/handle an error. If there is an errir, we will handle it in a separate fucntion aside from our result. 
	*/
	newTask.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
	
})

//GET route - get all tasks documents

app.get('/tasks',(req, res)=>{

	//res.send("Testing from get all task documents route")

	//to be able to find() or get all documents from a collection, we will use the find() method of our model
	//mongoDB equivalent - db.tasks.find({})

	Task.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
})

/*
	Activity 1

		Create a new schema for User. It should have the following fields:
			username,password.
		The data types for both fields is String.

		Create a new model out of your schema and save it in a variable called User

		Create a new POST method route to create a new user document:
			-endpoint: "/users"
			-This route should be able to create a new user document.
			-Then, send the result in the client.
			-Catch an error while saving, send the error in the client.

		Stretch Goal:

		Create a new GET method route to retrieve all user documents:
			-endpoint: "/users"
			-Then, send the result in the client.
			-Catch an error, send the error in the client.
*/

const userSchema = new mongoose.Schema({

	username: String,
	password: String 
})

let User = mongoose.model("User",userSchema);

app.post('/users',(req,res)=>{

	let newUser = new User({
		username: req.body.username,
		password: req.body.password
	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))
})

//GET route - get all user documents

app.get('/users',(req, res)=>{

	//res.send("Testing from get all task documents route")

	//to be able to find() or get all documents from a collection, we will use the find() method of our model
	//mongoDB equivalent - db.tasks.find({})

	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
})

//Start the server and confirm that it is running
app.listen(port, () => console.log(`Server is running: ${port}`))